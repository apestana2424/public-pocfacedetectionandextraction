﻿using DlibDotNet;
using DlibDotNet.Extensions;
using System;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace POCFaceDetectionAndExtraction
{
    public partial class POCFaceDetectionAndExtractionForm : Form
    {
        protected string FORM_TITLE = "POC Face Detection and Extraction";

        public POCFaceDetectionAndExtractionForm()
        {
            InitializeComponent();

            this.Text = FORM_TITLE;
        }

        #region Button event handlers

        private void openButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.bmp;*.dng;*.gif;*.jpg;*.png)|*.bmp;*.dng;*.gif;*.jpg;*.png|All files (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Cursor.Current = Cursors.WaitCursor;

                    using Array2D<RgbPixel> img = Dlib.LoadImage<RgbPixel>(openFileDialog.FileName);

                    using FrontalFaceDetector ffd = Dlib.GetFrontalFaceDetector();
                    DlibDotNet.Rectangle[] faces = ffd.Operator(img);

                    if (faces.Length > 0)
                    {
                        using Array2D<RgbPixel> onlyFaceImg = ExtractFace(img, faces[0]);
                        this.extractedPictureBox.Image = onlyFaceImg.ToBitmap<RgbPixel>();

                        Dlib.DrawRectangle(img, faces[0], new RgbPixel(0, 255, 0), 8);
                        for (int i = 1; i < faces.Length; i++)
                        {
                            Dlib.DrawRectangle(img, faces[i], new RgbPixel(255, 0, 0), 8);
                        }
                        this.originalPictureBox.Image = img.ToBitmap<RgbPixel>();

                        if (faces.Length > 1)
                        {
                            MessageBox.Show("More than one face was detected - extracted the first face detected.", FORM_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        this.originalPictureBox.Image = img.ToBitmap<RgbPixel>();
                        this.extractedPictureBox.Image = null;
                        MessageBox.Show("No face was detected.", FORM_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Concat("Error: ", ex.Message), FORM_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.extractedPictureBox.Image == null)
                {
                    MessageBox.Show("No image to save.", FORM_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PNG image (*.png)|*.png|All files (*.*)|*.*";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Cursor.Current = Cursors.WaitCursor;

                    this.extractedPictureBox.Image.Save(saveFileDialog.FileName, ImageFormat.Png);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Concat("Error: ", ex.Message), FORM_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                string.Concat("This is a POC on face detection and extraction.",
                    " The goal is to generate properly cut ID photos from loosely taken photos.",
                    Environment.NewLine, Environment.NewLine,
                    "The library used was DlibDotNet (https://github.com/takuya-takeuchi/DlibDotNet), a .NET Core wrapper to Dlib (http://dlib.net/), the popular C++ machine learning toolkit, that made it really easy to accomplish the goal."
                ), FORM_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion Button event handlers

        protected static Array2D<RgbPixel> ExtractFace(Array2D<RgbPixel> img, DlibDotNet.Rectangle faceRectangle)
        {
            const float WIDTH_MULTIPLICATION_FACTOR = 1.5f; // On the extracted face we need more of the image
                                                            // than the detected face itself.
            const float HEIGHT_TO_WIDTH_RATIO = 1.3f; // We want an identification photo, which is taller than
                                                      // wider. This is the height to width ratio we want.

            // Determine the width and height of the face, as detected by Dlib.
            int faceWidth = faceRectangle.TopRight.X - faceRectangle.TopLeft.X;
            int faceHeight = faceRectangle.BottomLeft.Y - faceRectangle.TopLeft.Y;

            // Calculate the width and height of the final photo with the extracted face.
            int finalWidth = (int)(faceWidth * WIDTH_MULTIPLICATION_FACTOR);
            int finalHeight = (int)(finalWidth * HEIGHT_TO_WIDTH_RATIO);

            // We want the final photo to have more to the left, right, top and bottom than
            // the area detected by Dlib as being the face. Here, we determine how much
            // we want to take extra from each side.
            int horizontalAddition = (finalWidth - faceWidth) / 2;
            int verticalAddition = (finalHeight - faceHeight) / 2;

            // Start with the coordinates given by Dlib.
            int xLeft = faceRectangle.TopLeft.X;
            int xRight = faceRectangle.TopRight.X;
            int yTop = faceRectangle.TopLeft.Y;
            int yBottom = faceRectangle.BottomLeft.Y;

            // Go a little bit more to the left, right, top and bottom, taking
            // care not to go out of the original image.
            xLeft -= horizontalAddition;
            if (xLeft < 0)
            {
                xLeft = 0;
            }
            xRight += horizontalAddition;
            if (xRight > img.Columns)
            {
                xRight = img.Columns;
            }
            yTop -= verticalAddition;
            if (yTop < 0)
            {
                yTop = 0;
            }
            yBottom += verticalAddition;
            if (yBottom > img.Rows)
            {
                yBottom = img.Rows;
            }

            DPoint[] points = new DPoint[]
            {
                new DPoint(xLeft, yTop),
                new DPoint(xRight, yTop ),
                new DPoint(xRight, yBottom),
                new DPoint(xLeft, yBottom)
            };

            return Dlib.ExtractImage4Points<RgbPixel>(img, points, finalWidth, finalHeight);
        }
    }
}