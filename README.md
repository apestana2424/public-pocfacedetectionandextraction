# POC Face Detection and Extraction #

### What is this? ###

This is a POC on face detection and extraction. The goal is to generate properly cut ID photos from loosely taken photos.

The POC was implemented as a .NET Core WinForms application and the library used was [DlibDotNet](https://github.com/takuya-takeuchi/DlibDotNet), a .NET Core wrapper to [Dlib](http://dlib.net/), the popular C++ machine learning toolkit, that made it really easy to accomplish the goal.

![Screenshot](screenshot.png)

### License ###

Copyright 2020 Antonio Pestana

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
